一、文件目录结构
|———dist
	|————css(压缩后的样式文件)
	|————js(合并后的js文件和压缩后的js文件)
	|————images(压缩后的图片文件和精灵图文件)
	|————fonts(图标字体文件)
	|————*.html(其他html文件)
|———html(html文件)
|———images(存放公共图片资源)
|———js(js文件)
	|————component(js组件)
	|————*.js(业务js)
|———sass(样式文件)
	|————options(样式组件)
	|————*.css(总样式)
|———spriteIcon(精灵图图标*.png)
|———svg(图标字体*.svg)

二、使用方法
1、打开命令行
	$ cnpm install -g gulp
	$ cnpm install
	$ gulp