var gulp = require('gulp');
var sass = require('gulp-sass');//sass工具
var minifycss = require('gulp-minify-css');//压缩样式工具
var concat = require('gulp-concat');//合并工具
var rename = require('gulp-rename');//重命名工具
var uglify = require('gulp-uglify');//压缩工具
var imagemin = require('gulp-imagemin');//压缩图片工具
var spritesmith = require('gulp.spritesmith');//精灵图工具
var iconfont = require('gulp-iconfont');
var iconfontCss = require('gulp-iconfont-css');
var fileinclude = require('gulp-file-include');//html模板工具
var browserSync = require('browser-sync').create();//链接工具

var fontName = 'Icons';
var runTimestamp = Math.round(Date.now()/1000);

gulp.task('serve', ['sass', 'js', 'sprite', 'imagemin','iconfont','html','serviceJs'], function () {
    browserSync.init({
        server: "./dist"
    });
    gulp.watch('sass/**/*.scss', ['sass']);
    gulp.watch('js/mui/*.js', ['js']);
    gulp.watch('js/*.js', ['serviceJs']);
    gulp.watch('spriteIcon/*.png', ['sprite']);
    gulp.watch('svg/*.svg', ['iconfont']);
    gulp.watch('html/*.html', ['html']);
    gulp.watch('images/*.{jpg,png}', ['imagemin']);
    // gulp.watch("src/*.html" ,browserSync.reload);
    gulp.watch("dist/*.html").on('change', browserSync.reload);
});

gulp.task('html', function () {
    return gulp.src('html/*.html')
    .pipe(fileinclude())
    .pipe(gulp.dest('dist/'));
});

gulp.task('iconfont', function(){
  return gulp.src(['svg/*.svg'])
    .pipe(iconfontCss({
      fontName: fontName,
      path: 'sass/options/_svgIcons.scss',//模板路径
      targetPath: '../../sass/options/_icons.scss', //生成的iconfont scss路径(相对字体生成的路径)
      fontPath: '../fonts/' //字体相对资源的引用路径
    }))
    .pipe(iconfont({
      fontName: fontName,
      prependUnicode: true, // recommended option
      formats: ['ttf', 'eot', 'woff','woff2','svg'],
      timestamp: runTimestamp,
      normalize:true,
      fontHeight: 1001
     }))
    .pipe(gulp.dest('dist/fonts/'));
});

gulp.task('sprite', function () {
    var spriteData = gulp.src('spriteIcon/*.png').pipe(spritesmith({
        imgName: '../../images/sprite.png',
        cssName: '_sprite.scss',
        cssFormat: 'scss',
        cssVarMap: function (sprite) {
            sprite.name = 'icon-' + sprite.name
        }
    }));
    spriteData.img.pipe(gulp.dest('dist/images'));
    spriteData.css.pipe(gulp.dest('sass/options/'));
});

gulp.task('sass', function () {
    return gulp.src('sass/**/*.scss')
        .pipe(sass())
        .pipe(minifycss())
        .pipe(gulp.dest('dist/css'));
});

gulp.task('js', function () {
    return gulp.src([
          'js/component/*.js'
        ])
        .pipe(concat('script.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(uglify())
        .pipe(rename('script.min.js'))
        .pipe(gulp.dest('dist/js'));

        
});

gulp.task('serviceJs', function () {
   gulp.src('js/*.js')
        .pipe(concat('zhxcf.js'))
        .pipe(gulp.dest('dist/js'))
        .pipe(uglify())
        .pipe(rename('zhxcf.min.js'))
        .pipe(gulp.dest('dist/js'));
});
gulp.task('imagemin', function () {
    return gulp.src('images/*.{jpg,png}')
        .pipe(imagemin())
        .pipe(gulp.dest('dist/images'));
});



gulp.task('default', ['serve']);